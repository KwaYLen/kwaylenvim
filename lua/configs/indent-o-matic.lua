local indent_o_matic = require "indent-o-matic"
indent_o_matic.setup(KwaYLenVim.user_plugin_opts "plugins.indent-o-matic")
indent_o_matic.detect()
