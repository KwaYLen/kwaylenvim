local dap = require "dap"
dap.adapters = KwaYLenVim.user_plugin_opts("dap.adapters", dap.adapters)
dap.configurations = KwaYLenVim.user_plugin_opts("dap.configurations", dap.configurations)
