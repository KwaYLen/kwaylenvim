require("neo-tree").setup(KwaYLenVim.user_plugin_opts("plugins.neo-tree", {
  close_if_last_window = true,
  enable_diagnostics = false,
  source_selector = {
    winbar = true,
    content_layout = "center",
    tab_labels = {
      filesystem = KwaYLenVim.get_icon "FolderClosed" .. " File",
      buffers = KwaYLenVim.get_icon "DefaultFile" .. " Bufs",
      git_status = KwaYLenVim.get_icon "Git" .. " Git",
      diagnostics = KwaYLenVim.get_icon "Diagnostic" .. " Diagnostic",
    },
  },
  default_component_configs = {
    indent = { padding = 0 },
    icon = {
      folder_closed = KwaYLenVim.get_icon "FolderClosed",
      folder_open = KwaYLenVim.get_icon "FolderOpen",
      folder_empty = KwaYLenVim.get_icon "FolderEmpty",
      default = KwaYLenVim.get_icon "DefaultFile",
    },
    git_status = {
      symbols = {
        added = KwaYLenVim.get_icon "GitAdd",
        deleted = KwaYLenVim.get_icon "GitDelete",
        modified = KwaYLenVim.get_icon "GitChange",
        renamed = KwaYLenVim.get_icon "GitRenamed",
        untracked = KwaYLenVim.get_icon "GitUntracked",
        ignored = KwaYLenVim.get_icon "GitIgnored",
        unstaged = KwaYLenVim.get_icon "GitUnstaged",
        staged = KwaYLenVim.get_icon "GitStaged",
        conflict = KwaYLenVim.get_icon "GitConflict",
      },
    },
  },
  window = {
    width = 30,
    mappings = {
      ["<space>"] = false, -- disable space until we figure out which-key disabling
      o = "open",
      H = "prev_source",
      L = "next_source",
    },
  },
  filesystem = {
    follow_current_file = true,
    hijack_netrw_behavior = "open_current",
    use_libuv_file_watcher = true,
    window = {
      mappings = {
        O = "system_open",
        h = "toggle_hidden",
      },
    },
    commands = {
      system_open = function(state) KwaYLenVim.system_open(state.tree:get_node():get_id()) end,
    },
  },
  event_handlers = {
    { event = "neo_tree_buffer_enter", handler = function(_) vim.opt_local.signcolumn = "auto" end },
  },
}))
