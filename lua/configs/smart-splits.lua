require("smart-splits").setup(KwaYLenVim.user_plugin_opts("plugins.smart-splits", {
  ignored_filetypes = {
    "nofile",
    "quickfix",
    "qf",
    "prompt",
  },
  ignored_buftypes = { "nofile" },
}))
