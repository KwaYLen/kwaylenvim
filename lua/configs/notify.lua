local notify = require "notify"
notify.setup(KwaYLenVim.user_plugin_opts("plugins.notify", { stages = "fade" }))
vim.notify = notify
