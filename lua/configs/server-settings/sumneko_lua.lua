return {
  settings = {
    Lua = {
      telemetry = { enable = false },
      runtime = { version = "LuaJIT" },
      diagnostics = { globals = { "vim", "KwaYLenVim", "KwaYLenVim_installation", "packer_plugins", "bit" } },
      workspace = {
        library = {
          vim.fn.expand "$VIMRUNTIME/lua",
          KwaYLenVim.install.home .. "/lua",
          KwaYLenVim.install.config .. "/lua",
        },
      },
    },
  },
}
