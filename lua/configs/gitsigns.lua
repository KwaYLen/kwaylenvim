require("gitsigns").setup(KwaYLenVim.user_plugin_opts("plugins.gitsigns", {
  signs = {
    add = { text = "▎" },
    change = { text = "▎" },
    delete = { text = "▎" },
    topdelete = { text = "契" },
    changedelete = { text = "▎" },
    untracked = { text = "▎" },
  },
}))
