local mason_lspconfig = require "mason-lspconfig"
mason_lspconfig.setup(KwaYLenVim.user_plugin_opts "plugins.mason-lspconfig")
mason_lspconfig.setup_handlers(
  KwaYLenVim.user_plugin_opts("mason-lspconfig.setup_handlers", { function(server) KwaYLenVim.lsp.setup(server) end })
)
KwaYLenVim.event "LspSetup"
