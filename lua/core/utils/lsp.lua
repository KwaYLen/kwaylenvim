--- ### KwaYLenVim LSP
--
-- This module is automatically loaded by KwaYLenVim on during it's initialization into global variable `KwaYLenVim.lsp`
--
-- This module can also be manually loaded with `local updater = require("core.utils").lsp`
--
-- @module core.utils.lsp
-- @see core.utils
-- @copyright 2022
-- @license GNU General Public License v3.0

KwaYLenVim.lsp = {}
local tbl_contains = vim.tbl_contains
local tbl_isempty = vim.tbl_isempty
local user_plugin_opts = KwaYLenVim.user_plugin_opts
local conditional_func = KwaYLenVim.conditional_func
local is_available = KwaYLenVim.is_available
local user_registration = user_plugin_opts("lsp.server_registration", nil, false)
local skip_setup = user_plugin_opts "lsp.skip_setup"

KwaYLenVim.lsp.formatting =
  KwaYLenVim.user_plugin_opts("lsp.formatting", { format_on_save = { enabled = true }, disabled = {} })
if type(KwaYLenVim.lsp.formatting.format_on_save) == "boolean" then
  KwaYLenVim.lsp.formatting.format_on_save = { enabled = KwaYLenVim.lsp.formatting.format_on_save }
end

KwaYLenVim.lsp.format_opts = vim.deepcopy(KwaYLenVim.lsp.formatting)
KwaYLenVim.lsp.format_opts.disabled = nil
KwaYLenVim.lsp.format_opts.format_on_save = nil
KwaYLenVim.lsp.format_opts.filter = function(client)
  local filter = KwaYLenVim.lsp.formatting.filter
  local disabled = KwaYLenVim.lsp.formatting.disabled or {}
  -- check if client is fully disabled or filtered by function
  return not (vim.tbl_contains(disabled, client.name) or (type(filter) == "function" and not filter(client)))
end

--- Helper function to set up a given server with the Neovim LSP client
-- @param server the name of the server to be setup
KwaYLenVim.lsp.setup = function(server)
  if not tbl_contains(skip_setup, server) then
    -- if server doesn't exist, set it up from user server definition
    if not pcall(require, "lspconfig.server_configurations." .. server) then
      local server_definition = user_plugin_opts("lsp.server-settings." .. server)
      if server_definition.cmd then require("lspconfig.configs")[server] = { default_config = server_definition } end
    end
    local opts = KwaYLenVim.lsp.server_settings(server)
    if type(user_registration) == "function" then
      user_registration(server, opts)
    else
      require("lspconfig")[server].setup(opts)
    end
  end
end

--- The `on_attach` function used by KwaYLenVim
-- @param client the LSP client details when attaching
-- @param bufnr the number of the buffer that the LSP client is attaching to
KwaYLenVim.lsp.on_attach = function(client, bufnr)
  local capabilities = client.server_capabilities
  local lsp_mappings = {
    n = {
      ["<leader>ld"] = { function() vim.diagnostic.open_float() end, desc = "Hover diagnostics" },
      ["[d"] = { function() vim.diagnostic.goto_prev() end, desc = "Previous diagnostic" },
      ["]d"] = { function() vim.diagnostic.goto_next() end, desc = "Next diagnostic" },
      ["gl"] = { function() vim.diagnostic.open_float() end, desc = "Hover diagnostics" },
    },
    v = {},
  }

  if is_available "mason-lspconfig.nvim" then
    lsp_mappings.n["<leader>li"] = { "<cmd>LspInfo<cr>", desc = "LSP information" }
  end

  if is_available "null-ls.nvim" then
    lsp_mappings.n["<leader>lI"] = { "<cmd>NullLsInfo<cr>", desc = "Null-ls information" }
  end

  if capabilities.codeActionProvider then
    lsp_mappings.n["<leader>la"] = { function() vim.lsp.buf.code_action() end, desc = "LSP code action" }
    lsp_mappings.v["<leader>la"] = lsp_mappings.n["<leader>la"]
  end

  if capabilities.codeLensProvider then
    lsp_mappings.n["<leader>ll"] = { function() vim.lsp.codelens.refresh() end, desc = "LSP codelens refresh" }
    lsp_mappings.n["<leader>lL"] = { function() vim.lsp.codelens.run() end, desc = "LSP codelens run" }
  end

  if capabilities.declarationProvider then
    lsp_mappings.n["gD"] = { function() vim.lsp.buf.declaration() end, desc = "Declaration of current symbol" }
  end

  if capabilities.definitionProvider then
    lsp_mappings.n["gd"] = { function() vim.lsp.buf.definition() end, desc = "Show the definition of current symbol" }
  end

  if capabilities.documentFormattingProvider and not tbl_contains(KwaYLenVim.lsp.formatting.disabled, client.name) then
    lsp_mappings.n["<leader>lf"] = {
      function() vim.lsp.buf.format(KwaYLenVim.lsp.format_opts) end,
      desc = "Format buffer",
    }
    lsp_mappings.v["<leader>lf"] = lsp_mappings.n["<leader>lf"]

    vim.api.nvim_buf_create_user_command(
      bufnr,
      "Format",
      function() vim.lsp.buf.format(KwaYLenVim.lsp.format_opts) end,
      { desc = "Format file with LSP" }
    )
    local autoformat = KwaYLenVim.lsp.formatting.format_on_save
    local filetype = vim.api.nvim_buf_get_option(bufnr, "filetype")
    if
      autoformat.enabled
      and (tbl_isempty(autoformat.allow_filetypes or {}) or tbl_contains(autoformat.allow_filetypes, filetype))
      and (tbl_isempty(autoformat.ignore_filetypes or {}) or not tbl_contains(autoformat.ignore_filetypes, filetype))
    then
      local autocmd_group = "auto_format_" .. bufnr
      vim.api.nvim_create_augroup(autocmd_group, { clear = true })
      vim.api.nvim_create_autocmd("BufWritePre", {
        group = autocmd_group,
        buffer = bufnr,
        desc = "Auto format buffer " .. bufnr .. " before save",
        callback = function()
          if vim.g.autoformat_enabled then
            vim.lsp.buf.format(KwaYLenVim.default_tbl({ bufnr = bufnr }, KwaYLenVim.lsp.format_opts))
          end
        end,
      })
      lsp_mappings.n["<leader>uf"] = {
        function() KwaYLenVim.ui.toggle_autoformat() end,
        desc = "Toggle autoformatting",
      }
    end
  end

  if capabilities.documentHighlightProvider then
    local highlight_name = vim.fn.printf("lsp_document_highlight_%d", bufnr)
    vim.api.nvim_create_augroup(highlight_name, {})
    vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
      group = highlight_name,
      buffer = bufnr,
      callback = function() vim.lsp.buf.document_highlight() end,
    })
    vim.api.nvim_create_autocmd("CursorMoved", {
      group = highlight_name,
      buffer = bufnr,
      callback = function() vim.lsp.buf.clear_references() end,
    })
  end

  if capabilities.hoverProvider then
    lsp_mappings.n["K"] = { function() vim.lsp.buf.hover() end, desc = "Hover symbol details" }
  end

  if capabilities.implementationProvider then
    lsp_mappings.n["gI"] = { function() vim.lsp.buf.implementation() end, desc = "Implementation of current symbol" }
  end

  if capabilities.referencesProvider then
    lsp_mappings.n["gr"] = { function() vim.lsp.buf.references() end, desc = "References of current symbol" }
    lsp_mappings.n["<leader>lR"] = { function() vim.lsp.buf.references() end, desc = "Search references" }
  end

  if capabilities.renameProvider then
    lsp_mappings.n["<leader>lr"] = { function() vim.lsp.buf.rename() end, desc = "Rename current symbol" }
  end

  if capabilities.signatureHelpProvider then
    lsp_mappings.n["<leader>lh"] = { function() vim.lsp.buf.signature_help() end, desc = "Signature help" }
  end

  if capabilities.typeDefinitionProvider then
    lsp_mappings.n["gT"] = { function() vim.lsp.buf.type_definition() end, desc = "Definition of current type" }
  end

  if capabilities.workspaceSymbolProvider then
    lsp_mappings.n["<leader>lG"] = { function() vim.lsp.buf.workspace_symbol() end, desc = "Search workspace symbols" }
  end

  if is_available "telescope.nvim" then -- setup telescope mappings if available
    if lsp_mappings.n.gd then lsp_mappings.n.gd[1] = function() require("telescope.builtin").lsp_definitions() end end
    if lsp_mappings.n.gI then
      lsp_mappings.n.gI[1] = function() require("telescope.builtin").lsp_implementations() end
    end
    if lsp_mappings.n.gr then lsp_mappings.n.gr[1] = function() require("telescope.builtin").lsp_references() end end
    if lsp_mappings.n["<leader>lR"] then
      lsp_mappings.n["<leader>lR"][1] = function() require("telescope.builtin").lsp_references() end
    end
    if lsp_mappings.n.gT then
      lsp_mappings.n.gT[1] = function() require("telescope.builtin").lsp_type_definitions() end
    end
    if lsp_mappings.n["<leader>lG"] then
      lsp_mappings.n["<leader>lG"][1] = function() require("telescope.builtin").lsp_workspace_symbols() end
    end
  end

  KwaYLenVim.set_mappings(user_plugin_opts("lsp.mappings", lsp_mappings), { buffer = bufnr })
  if not vim.tbl_isempty(lsp_mappings.v) then
    KwaYLenVim.which_key_register({ v = { ["<leader>"] = { l = { name = "LSP" } } } }, { buffer = bufnr })
  end

  local on_attach_override = user_plugin_opts("lsp.on_attach", nil, false)
  conditional_func(on_attach_override, true, client, bufnr)
end

--- The default KwaYLenVim LSP capabilities
KwaYLenVim.lsp.capabilities = vim.lsp.protocol.make_client_capabilities()
KwaYLenVim.lsp.capabilities.textDocument.completion.completionItem.documentationFormat = { "markdown", "plaintext" }
KwaYLenVim.lsp.capabilities.textDocument.completion.completionItem.snippetSupport = true
KwaYLenVim.lsp.capabilities.textDocument.completion.completionItem.preselectSupport = true
KwaYLenVim.lsp.capabilities.textDocument.completion.completionItem.insertReplaceSupport = true
KwaYLenVim.lsp.capabilities.textDocument.completion.completionItem.labelDetailsSupport = true
KwaYLenVim.lsp.capabilities.textDocument.completion.completionItem.deprecatedSupport = true
KwaYLenVim.lsp.capabilities.textDocument.completion.completionItem.commitCharactersSupport = true
KwaYLenVim.lsp.capabilities.textDocument.completion.completionItem.tagSupport = { valueSet = { 1 } }
KwaYLenVim.lsp.capabilities.textDocument.completion.completionItem.resolveSupport = {
  properties = { "documentation", "detail", "additionalTextEdits" },
}
KwaYLenVim.lsp.capabilities = user_plugin_opts("lsp.capabilities", KwaYLenVim.lsp.capabilities)
KwaYLenVim.lsp.flags = user_plugin_opts "lsp.flags"

--- Get the server settings for a given language server to be provided to the server's `setup()` call
-- @param  server_name the name of the server
-- @return the table of LSP options used when setting up the given language server
function KwaYLenVim.lsp.server_settings(server_name)
  local server = require("lspconfig")[server_name]
  local opts = user_plugin_opts( -- get user server-settings
    "lsp.server-settings." .. server_name, -- TODO: RENAME lsp.server-settings to lsp.config in v3
    user_plugin_opts("server-settings." .. server_name, { -- get default server-settings
      capabilities = vim.tbl_deep_extend("force", KwaYLenVim.lsp.capabilities, server.capabilities or {}),
      flags = vim.tbl_deep_extend("force", KwaYLenVim.lsp.flags, server.flags or {}),
    }, true, "configs")
  )
  local old_on_attach = server.on_attach
  local user_on_attach = opts.on_attach
  opts.on_attach = function(client, bufnr)
    conditional_func(old_on_attach, true, client, bufnr)
    KwaYLenVim.lsp.on_attach(client, bufnr)
    conditional_func(user_on_attach, true, client, bufnr)
  end
  return opts
end

return KwaYLenVim.lsp
