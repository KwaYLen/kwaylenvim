--- ### KwaYLenVim Mason Utils
--
-- This module is automatically loaded by KwaYLenVim on during it's initialization into global variable `KwaYLenVim.mason`
--
-- This module can also be manually loaded with `local updater = require("core.utils").mason`
--
-- @module core.utils.mason
-- @see core.utils
-- @copyright 2022
-- @license GNU General Public License v3.0

KwaYLenVim.mason = {}

--- Update a mason package
-- @param pkg_name string of the name of the package as defined in Mason (Not mason-lspconfig or mason-null-ls)
-- @param auto_install boolean of whether or not to install a package that is not currently installed (default: True)
function KwaYLenVim.mason.update(pkg_name, auto_install)
  if auto_install == nil then auto_install = true end
  local registry_avail, registry = pcall(require, "mason-registry")
  if not registry_avail then
    vim.api.nvim_err_writeln "Unable to access mason registry"
    return
  end

  local pkg_avail, pkg = pcall(registry.get_package, pkg_name)
  if not pkg_avail then
    KwaYLenVim.notify(("Mason: %s is not available"):format(pkg_name), "error")
  else
    if not pkg:is_installed() then
      if auto_install then
        KwaYLenVim.notify(("Mason: Installing %s"):format(pkg.name))
        pkg:install()
      else
        KwaYLenVim.notify(("Mason: %s not installed"):format(pkg.name), "warn")
      end
    else
      pkg:check_new_version(function(update_available, version)
        if update_available then
          KwaYLenVim.notify(("Mason: Updating %s to %s"):format(pkg.name, version.latest_version))
          pkg:install():on("closed", function() KwaYLenVim.notify(("Mason: Updated %s"):format(pkg.name)) end)
        else
          KwaYLenVim.notify(("Mason: No updates available for %s"):format(pkg.name))
        end
      end)
    end
  end
end

--- Update all packages in Mason
function KwaYLenVim.mason.update_all()
  local registry_avail, registry = pcall(require, "mason-registry")
  if not registry_avail then
    vim.api.nvim_err_writeln "Unable to access mason registry"
    return
  end

  local installed_pkgs = registry.get_installed_packages()
  local running = #installed_pkgs
  local no_pkgs = running == 0
  KwaYLenVim.notify "Mason: Checking for package updates..."

  if no_pkgs then
    KwaYLenVim.notify "Mason: No updates available"
    KwaYLenVim.event "MasonUpdateComplete"
  else
    local updated = false
    for _, pkg in ipairs(installed_pkgs) do
      pkg:check_new_version(function(update_available, version)
        if update_available then
          updated = true
          KwaYLenVim.notify(("Mason: Updating %s to %s"):format(pkg.name, version.latest_version))
          pkg:install():on("closed", function()
            running = running - 1
            if running == 0 then
              KwaYLenVim.notify "Mason: Update Complete"
              KwaYLenVim.event "MasonUpdateComplete"
            end
          end)
        else
          running = running - 1
          if running == 0 then
            if updated then
              KwaYLenVim.notify "Mason: Update Complete"
            else
              KwaYLenVim.notify "Mason: No updates available"
            end
            KwaYLenVim.event "MasonUpdateComplete"
          end
        end
      end)
    end
  end
end

return KwaYLenVim.mason
