local signs = {
  { name = "DiagnosticSignError", text = KwaYLenVim.get_icon "DiagnosticError" },
  { name = "DiagnosticSignWarn", text = KwaYLenVim.get_icon "DiagnosticWarn" },
  { name = "DiagnosticSignHint", text = KwaYLenVim.get_icon "DiagnosticHint" },
  { name = "DiagnosticSignInfo", text = KwaYLenVim.get_icon "DiagnosticInfo" },
  { name = "DiagnosticSignError", text = KwaYLenVim.get_icon "DiagnosticError" },
  { name = "DapStopped", text = KwaYLenVim.get_icon "DapStopped", texthl = "DiagnosticWarn" },
  { name = "DapBreakpoint", text = KwaYLenVim.get_icon "DapBreakpoint", texthl = "DiagnosticInfo" },
  { name = "DapBreakpointRejected", text = KwaYLenVim.get_icon "DapBreakpointRejected", texthl = "DiagnosticError" },
  { name = "DapBreakpointCondition", text = KwaYLenVim.get_icon "DapBreakpointCondition", texthl = "DiagnosticInfo" },
  { name = "DapLogPoint", text = KwaYLenVim.get_icon "DapLogPoint", texthl = "DiagnosticInfo" },
}

for _, sign in ipairs(signs) do
  if not sign.texthl then sign.texthl = sign.name end
  vim.fn.sign_define(sign.name, sign)
end

KwaYLenVim.lsp.diagnostics = {
  off = {
    underline = false,
    virtual_text = false,
    signs = false,
    update_in_insert = false,
  },
  on = KwaYLenVim.user_plugin_opts("diagnostics", {
    virtual_text = true,
    signs = { active = signs },
    update_in_insert = true,
    underline = true,
    severity_sort = true,
    float = {
      focused = false,
      style = "minimal",
      border = "rounded",
      source = "always",
      header = "",
      prefix = "",
    },
  }),
}

vim.diagnostic.config(KwaYLenVim.lsp.diagnostics[vim.g.diagnostics_enabled and "on" or "off"])

